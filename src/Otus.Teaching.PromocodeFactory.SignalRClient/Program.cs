﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace Otus.Teaching.PromocodeFactory.SignalRClient
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/hubs/customers")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();

            connection.On("CustomerUpdated",
                (string customerId) => { Console.WriteLine($"Customer with Id = {customerId} has been updated"); });

            var customer =
                await connection.InvokeAsync<CustomerResponse>("GetCustomerAsync",
                    "a6c8c6b1-4349-45b0-ab31-244740aaf0f0");

            var statusCode = await connection.InvokeAsync<string>("EditCustomersAsync",
                "a6c8c6b1-4349-45b0-ab31-244740aaf0f0", new
                {
                    FirstName = "TestFirstName",
                    LastName = "TestFirstName",
                    Email = "test@example.com",
                    PreferenceIds = new List<Guid>()
                });


            Console.ReadLine();
            await connection.StopAsync();
        }
    }
}