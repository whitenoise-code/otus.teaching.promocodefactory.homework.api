using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;

namespace Otus.Teaching.PromocodeFactory.GrpcServiceClient
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            //var client = new Customer.CustomerClient(channel);
            var client = new Customers.CustomersClient(channel);
            var reply = await client.GetAllCustomersAsync(new Empty());
        }
    }
}