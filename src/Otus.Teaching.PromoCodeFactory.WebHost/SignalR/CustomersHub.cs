﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.SignalR
{
    public class CustomersHub : Hub<ICustomersServiceClient>
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        private const string CustomersClients = "Customers clients";

        public CustomersHub(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task OnConnectedAsync()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, CustomersClients);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, CustomersClients);
            await base.OnDisconnectedAsync(exception);
        }

        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return response;
        }

        public async Task<CustomerResponse> GetCustomerAsync(string id)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(id));

            var response = new CustomerResponse(customer);

            return response;
        }

        public async Task<string> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            var customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return customer.Id.ToString();
        }

        public async Task<string> EditCustomersAsync(string id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(id));

            if (customer == null)
                return "NotFound";

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            //Сообщаяем всем клиентам id изменённого покупателя
            await Clients.Group(CustomersClients).CustomerUpdated(id);

            return "Accepted";
        }

        public async Task<string> DeleteCustomerAsync(string id)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(id));

            if (customer == null)
                return "NotFound";

            await _customerRepository.DeleteAsync(customer);

            return "NoContent";
        }
    }
}