﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.SignalR
{
    public interface ICustomersServiceClient
    {
        Task CustomerUpdated(string customerId);
    }
}