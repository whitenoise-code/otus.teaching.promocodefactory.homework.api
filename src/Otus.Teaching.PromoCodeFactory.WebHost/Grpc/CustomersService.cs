﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromocodeFactory.WebHost.Grpc;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Grpc
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<GetAllReply> GetAllCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortReply()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            var reply = new GetAllReply {StatusCode = StatusCodes.Status200OK};
            reply.Customers.Add(response);

            return reply;
        }

        public override async Task<CustomerReply> GetCustomer(GetRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            var reply = ToCustomerReply(customer);
            reply.StatusCode = StatusCodes.Status200OK;

            return reply;
        }

        public override async Task<CreateReply> CreateCustomer(CreateRequest request, ServerCallContext context)
        {
            var guids = request.PreferenceIds.Select(Guid.Parse).ToList();
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(guids);

            Customer customer = MapFromCreateRequest(request, preferences);

            await _customerRepository.AddAsync(customer);
            return new CreateReply {StatusCode = StatusCodes.Status201Created};
        }

        public override async Task<EditReply> EditCustomer(EditRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return new EditReply {StatusCode = StatusCodes.Status404NotFound};

            var guids = request.PreferenceIds.Select(Guid.Parse).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(guids);

            MapFromEditRequest(customer, request, preferences);

            await _customerRepository.UpdateAsync(customer);

            return new EditReply {StatusCode = StatusCodes.Status202Accepted};
        }

        public override async Task<DeleteReply> DeleteCustomer(DeleteRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return new DeleteReply {StatusCode = StatusCodes.Status404NotFound};

            await _customerRepository.DeleteAsync(customer);

            return new DeleteReply {StatusCode = StatusCodes.Status204NoContent};
        }

        private static CustomerReply ToCustomerReply(Customer customer)
        {
            var reply = new CustomerReply()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
            };
            var prefs = customer.Preferences.Select(x => new PreferenceReply()
            {
                Id = x.PreferenceId.ToString(),
                Name = x.Preference.Name
            }).ToList();
            reply.Preferences.AddRange(prefs);
            return reply;
        }

        private static Customer MapFromCreateRequest(CreateRequest request, IEnumerable<Preference> preferences)
        {
            var custId = Guid.NewGuid();
            var customer = new Customer()
            {
                Id = custId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference()
                {
                    CustomerId = custId,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };

            return customer;
        }

        private static void MapFromEditRequest(Customer customer, EditRequest request,
            IEnumerable<Preference> preferences)
        {
            var custId = Guid.Parse(request.Id);
            customer.Id = custId;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = custId,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
        }
    }
}